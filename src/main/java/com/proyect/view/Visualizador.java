package com.proyect.view;

import java.awt.Color;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


/*
import com.proyect.controller.Controlador;
import com.proyect.service.MonitoreableService;
import com.proyect.service.PruebaService;
import com.proyect.service.impl.MonitoreableServiceImpl;
import com.proyect.service.impl.PruebaServiceImpl;
*/
public class Visualizador extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldIp;
	//private Controlador conector = new Controlador();

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { Visualizador frame = new
	 * Visualizador(); frame.setVisible(true); } catch (Exception e) {
	 * e.printStackTrace(); } } }); }
	 */

	/**
	 * Create the frame.
	 */
	public Visualizador() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 855, 407);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldIp = new JTextField();
		textFieldIp.setText("127.0.0.1");
		textFieldIp.setBounds(10, 27, 96, 20);
		contentPane.add(textFieldIp);
		textFieldIp.setColumns(10);

		JLabel lblIpDelEquipo = new JLabel("IP DEL EQUIPO");
		lblIpDelEquipo.setBounds(10, 11, 96, 14);
		contentPane.add(lblIpDelEquipo);

		TextArea textAreaVisor = new TextArea();
		textAreaVisor.setBounds(10, 88, 819, 250);
		textAreaVisor.setBackground(Color.BLACK);
		textAreaVisor.setForeground(Color.GREEN);
		contentPane.add(textAreaVisor);

		/**
		 * Este bot�n deberia agregar un "monitoreable" a una cola del monitorizador a
		 * trav�s del "Controlador"
		 */
		JButton btnAgregar = new JButton("AGREGAR");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			/*	String ip = textFieldIp.getText();

				String ubicacionPing = "C:\\Windows\\System32\\PING.EXE";
				String ubicacionTelnet = "C:\\Desarrollos\\HERRAMIENTAS_PP2\\PORTQRY\\Script.bat";
				String ubicacionIperf = "C:\\Desarrollos\\HERRAMIENTAS_PP2\\IPERF\\iperf-3.1.3-win32\\iperf3.exe";

				textAreaVisor.setText(textAreaVisor.getText() + "\n" + ip);
				MonitoreableService equipoDeRed = new MonitoreableServiceImpl(ip);

				PruebaService pruebas = new PruebaServiceImpl();

				// Prueba de ping a host
				pruebas.agregarHerramientaDePrueba(ubicacionPing, ((MonitoreableServiceImpl) equipoDeRed).getIp());
				// Prueba de puerto abierto
				pruebas.agregarHerramientaDePrueba(ubicacionTelnet, ((MonitoreableServiceImpl) equipoDeRed).getIp() + " 80");
				// Prueba de ancho de banda
				pruebas.agregarHerramientaDePrueba(ubicacionIperf, "-c " + ((MonitoreableServiceImpl) equipoDeRed).getIp());

				equipoDeRed.agregarPrueba(pruebas);
				conector.agregarEquipoParaMonitorear(equipoDeRed);
				conector.setSalida(textAreaVisor, pruebas);
*/
			}
		});
		btnAgregar.setBounds(122, 26, 134, 23);
		contentPane.add(btnAgregar);

		JButton btnMonitorear = new JButton("MONITOREAR");
		btnMonitorear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textAreaVisor.setText(null);
			//	conector.mostrarResultados();
			}
		});
		btnMonitorear.setBounds(298, 26, 134, 23);
		contentPane.add(btnMonitorear);
	}
}