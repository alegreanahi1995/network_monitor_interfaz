package com.proyect.main;

import javax.swing.UIManager;

import com.proyect.view.Visualizador;

public class Main {

	public static void main(String[] args) {
		try
		{
			UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
		}
		catch (Exception e) {}
		Visualizador frame = new Visualizador();
		frame.setVisible(true);
	}
}